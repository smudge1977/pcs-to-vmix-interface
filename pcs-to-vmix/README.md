# Play Cricket Scorer to vMix & Companion interface

Reads [PCS (Play Cricket Scorer)](https://www.play-cricket.com/features-playcricketscorerpro) data files and updates [vMix](https://www.vmix.com/help24/index.htm?vMixTitleDesigner.html) title data for both legacy titles and GT Designer titles

[Intro to GT video](https://www.youtube.com/watch?v=emScjEUVrJc)
[Stingers](https://www.youtube.com/watch?v=uS6y6PWPiNI)

RC: https://test.pypi.org/project/pcs-to-vmix/  
Formal: https://pypi.org/project/pcs-to-vmix/  
[python interface](pcs-to-vmix/README.md)

## Installing

This should do the job for you:  
[runme.cmd](../runme.cmd)

# Install & Configuration
## Install
Adding `https://test.pypi.org/legacy/` to install rc rather than formal:
```
pip3 config list -v
```

Simply install with `pip install pcs-to-vmix` or from test PyPi with `pip install -i https://test.pypi.org/simple/ pcs-to-vmix`

## Change log

- 1.0.8 - Support for toggeling graphics in GT Titles
