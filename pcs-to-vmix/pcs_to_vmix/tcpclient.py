#!/usr/env python3
""" The main application.

The user entry point

```mermaid 
flowchart TD
thing --> another
```


"""

import logging
import asyncio
import os
import argparse
from datetime import datetime, timezone

from time import sleep

from lxml import etree as et

from pcs_to_vmix.version import VERSION
from pcs_to_vmix.vmix import Vmix8099ClientProtocol
from pcs_to_vmix.pcs import PCSScoreboardFileWatcher

log = logging.getLogger(__name__)



def main():
    """ Main entry point to the utility """
    
    formatter = logging.Formatter("%(asctime)s %(levelname)s " +
                                    "[%(module)s:%(lineno)d] %(message)s")
    
    # setup console logging
    log.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    ch.setFormatter(formatter)
    log.addHandler(ch)
    # from ..setup import setup
    log.debug(f'VERSION:{VERSION} | Debug log message')
    log.info(f'VERSION:{VERSION} | Info log message')
    log.warning(f'VERSION:{VERSION} | Warning log message')
    log.info(f'Available loggers:{logging.root.manager.loggerDict.keys()}')
    my_parser = argparse.ArgumentParser(description=''''Conect Play Cricket Scorer to vMIX''')
    # Add the arguments
    my_parser.add_argument('--path',
                        action='store',
                        type=str,
                        default='c:/vmix/livedata.xml',
                        help='PCS Scorboard Output file - see README.md for how to configure PCS')

    my_parser.add_argument('--vmixip',
                        action='store',
                        type=str,
                        default='127.0.0.1',
                        help='vMix IP')

    my_parser.add_argument('--vmixport',
                        action = 'store',
                        type = int,
                        default = 8099,
                        help='vMix port')

    my_parser.add_argument('--info',
                        action = 'store',
                        type = str,
                        default = "pcs_to_vmix,pcs_to_vmix.pcs,pcs_to_vmix.vmix",
                        help=f'Set log level to info for comma seperated list of loggers - {logging.root.manager.loggerDict.keys()}')
    my_parser.add_argument('--debug',
                        action = 'store',
                        type = str,
                        default = "",
                        help=f'Set log level to debug for comma seperated list of loggers - {logging.root.manager.loggerDict.keys()}')
    now = datetime.now(timezone.utc).timestamp()
    my_parser.add_argument('--logfile',
                        action = 'store',
                        type = str,
                        default = f'c:\\vmix\\pcs_to_vmix.{now}.log',
                        help = 'Log file name - you need to handle clean up else where maybe in a wrapper script?')
    # Execute the parse_args() method
    args = my_parser.parse_args()

    # TODO: handle module.* notation 
    for logger in args.info.split(','):
        logging.getLogger(logger.strip()).setLevel(logging.INFO)
    for logger in args.debug.split(','):
        logging.getLogger(logger.strip()).setLevel(logging.DEBUG)
    loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
    
    if args.logfile is not '':
        logging.basicConfig(filename=args.logfile, encoding='utf-8')

    log.info(f'Loggers setup:{loggers}')

    loop = asyncio.get_event_loop()

    input = PCSScoreboardFileWatcher(args.path)

    coro = loop.create_connection(lambda: Vmix8099ClientProtocol(loop, input),
                                args.vmixip, args.vmixport)
    
    loop.create_task(input.watch_for_file_change())
    loop.run_until_complete(coro)
    loop.run_forever()
    loop.close()


if __name__=="__main__":
    """ dumbar wrapper """
    main()