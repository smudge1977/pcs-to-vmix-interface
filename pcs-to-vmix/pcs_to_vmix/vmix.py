""" vMix Telnet 8099 API interface.  

This knows about title graphics and vMix things 
It knows nothing about cricket!
"""
import logging
import asyncio

from lxml import etree as et

log = logging.getLogger(__name__)


class Vmix8099ClientProtocol(asyncio.Protocol):
    """ The IP Session to vMix.  """
    def __init__(self, eventloop, coro):
        self.transport = None
        self.loop = eventloop
        self.xml = et.fromstring("<vmix/>")
        self.xmllength = 0
        self.xmlstr = ''
        self.coro = coro
        self._inProgress = '' # Set to type of MESSAGE if running into second TCP datagram (normally XML types as everything else is small)

    def connection_made(self, transport):
        """ Once connected get the XML.  

        coroutine (right name?) regsiter call backs for the field updater 
        - not sure these are all named in the best ways!
        """
        self.transport = transport

        self.transport.write('SUBSCRIBE ACTS\r\n'.encode())
        self.transport.write('SUBSCRIBE TALLY\r\n'.encode())
        self.transport.write('XML\r\n'.encode())
        if self.coro is not None:
            self.coro.registerCallBacks(self.update_field) # connect to the input file reader once connected to vMix
            self.coro.registerCallBacks(self.update_graphic_field)

    def data_received(self, byteBuffer):
        """ Prcoess TCP session incoming data.  """
        BLOCK_ENDING = b'\r\n'
        # strBuffer = byteBuffer.decode()

        log.debug(f'Data received: {byteBuffer}')
        # Prcess string Buffer until none left
        while len(byteBuffer) > 0:

            if byteBuffer.startswith(b'VERSION'):
                data, _, byteBuffer = byteBuffer.partition(BLOCK_ENDING)
                # buffer = buffer.encode() # back to bytes
                log.debug(f'VMIX VERSION :{data}')
                continue

            if byteBuffer.startswith(b'XML') or self._inProgress=='XML':
                data, self._inProgress, byteBuffer = vmixXMLdecoder.vmixXMLdecode(byteBuffer)
                if data is not None: # returns a None if in progress
                    # len(xml.getchildren()) > 0:
                    self.xml = data
                # else:
                #     self._inProgress = 'XML'
                continue

            # if buffer.decode().startswith('ACTS OK '):
            #     data, _, buffer = buffer.decode().partition(BLOCK_ENDING)
            #     log.debug(f'ACTS: {data}')
            #     continue

            # if buffer.decode().startswith('TALLY OK '):
            #     data, _, buffer = buffer.decode().partition('\r\n')
            #     log.debug(f'TALLY: {data}')
            #     continue

            # if buffer.decode().startswith('SUBSCRIBE OK '):
            #     data, _, buffer = buffer.decode().partition('\r\n')
            #     log.debug(f'SUBSCRIBE: {data}')
            #     continue

            if byteBuffer.startswith(b'FUNCTION OK '):
                data, _, byteBuffer = byteBuffer.partition(BLOCK_ENDING)
                # buffer = buffer.encode() # back to bytes
                log.debug(f'FUNCTION OK :{data}')
                continue

            if self._inProgress == '':
                data, _, byteBuffer = byteBuffer.partition(BLOCK_ENDING)
                log.warning(f'DATA:{data}| UNKOWN: Not sure... discard')
                continue
            


    # async def update_visable(self, field_name, value):
    #     log.debug(f'UPDATE_VISABLE: {field_name}, {value}')
    #     # for field in self.xml.xpath(f'.//image[starts-with(@name,"{field_name}.")]'):
    #     for field in self.xml.xpath(f'.//image[@name="{field_name}.Source"]'):
    #         input_number = field.getparent().attrib.get('number')
    #         if value.strip() == '':
    #             function_str = f'FUNCTION SetImageVisibleOff Input={input_number}&SelectedValue={field_name}.Source\r\n'
    #         else:
    #             function_str = f'FUNCTION SetImageVisibleOn Input={input_number}&SelectedValue={field_name}.Source\r\n'
    #             # await self.update_field(field_name, value)
    #         print(function_str)
    #         self.transport.write(function_str.encode())
    async def update_graphic_field(self, field_name, value):
        """ Field_name is the TCSldXXX or Bowl0 etc.
        
        ### TODO - use self.xml to determine if we actually need to send the update
        Or does this matter loadwise?
        Need to ensure we have an upto date xml though!"""

        # <image index="0" name="642 BowlSouthOn.Source"/>
        log.debug(f'update_graphic_field| field_name:{field_name}| value:{value}')
        # print(f'**FIELD NAME {field_name}, value:"{value}"')
        name = field_name[5:] + ' '
        for field in self.xml.xpath(f'.//image[starts-with(@name, "{name}")]'):
            input_number = field.getparent().attrib.get('number')
            index = field.attrib.get('index')
            if value.strip() == '':
                function_str = f'FUNCTION SetImageVisibleOff Input={input_number}&SelectedIndex={index}\r\n'
            else:
                function_str = f'FUNCTION SetImageVisibleOn Input={input_number}&SelectedIndex={index}\r\n'
            print(f'**{function_str}')
            self.transport.write(function_str.encode())

    async def update_field(self, field_name, value):
        """ Field_name is the TCSldXXX or Bowl0 etc.
        
        ### TODO - use self.xml to determine if we actually need to send the update
        Or does this matter loadwise?
        Need to ensure we have an upto date xml though!"""

        log.debug(f'update_field| field_name:{field_name}| value:{value}')
        
        # print(f'Print: UPDATE_FIELD: {field_name} {value}')

        # Support for legacy titles with legacy field naming:
        for field in self.xml.xpath(f'.//text[@name="{field_name}"]'):
        
                input_number = field.getparent().attrib.get('number')
                valueStrip = value.strip()
                function_str = f'FUNCTION SetText Input={input_number}&SelectedName={field_name}&Value={valueStrip}\r\n'

                log.info(f'UPDATE_FIELD| Found a legacy match in vmix input {input_number} {field_name}, setting to {valueStrip}')
                print(function_str)
                self.transport.write(function_str.encode())

        fieldNameList = [
            # field_name,
            field_name[5:] + ' ',
            field_name[5:] + '.',
        ]
        for name in fieldNameList:
            for field in self.xml.xpath(f'.//text[starts-with(@name, "{name}")]'):
                
                vmixname = field.attrib.get('name')
                input_number = field.getparent().attrib.get('number')
                valueStrip = value.strip()
                function_str = f'FUNCTION SetText Input={input_number}&SelectedName={vmixname}&Value={valueStrip}\r\n'

                log.info(f'UPDATE_FIELD| Found a match in vmix input {input_number} {vmixname} for {name} setting to {valueStrip}')
                print(function_str)
                self.transport.write(function_str.encode())
        
    def connection_lost(self, exc):
        log.critical('The server closed the connection')
        # print('Stop the event loop')
        # self.loop.stop()


class vmixXMLdecoder():
    """ # TODO - Make into a proper vMix_XML_Handler.  

    ie receivers parent substites a handler and then you always ask the handler for xml and don't have a local xml back in the client 
    """
    _xmlRemainingLength = 0
    _xmlBytes = '<vmix/>'

    @property
    def xml(self):
        return et.fromstring(self._xmlBytes)

    @classmethod
    def vmixXMLdecode(cls, buffer):
        """ return self.xml, inProgress, buffer
        buffer of xml data """
        log.debug(f'vmixXMLdecode| bytes:{buffer}| _xmllength:{cls._xmlRemainingLength}| _xmlstr:{cls._xmlBytes}')

        if buffer.startswith(b'XML'):
            cls._xmlBytes = bytearray()
            inProgress = 'XML'
            header, _, buffer = buffer.partition(b'\r\n')
            byteLength = header[4:] # drop the 'XML '<number of bytes>\r\n
            cls._xmlRemainingLength = int(byteLength)
            log.info(f'_xmllength:{cls._xmlRemainingLength}|len(buffer):{len(buffer)}')

        log.debug(f'str:{buffer}| len(buffer):{len(buffer)}| _xmllength:{cls._xmlRemainingLength}| _xmlstr:{cls._xmlBytes}')
        
        # if len(buffer) == 0:
        #     # TODO Should still have a valid xml to return here!
        #     xml = et.fromstring('<vmix/>')
        #     return xml, inProgress, buffer

        if len(buffer) < cls._xmlRemainingLength:
            log.warning(f'Not enounch in the buffer {len(buffer)} < {cls._xmlRemainingLength} so set inProgress and return')

            cls._xmlBytes = cls._xmlBytes + buffer
            cls._xmlRemainingLength -= len(buffer)
            inProgress = 'XML'
            
            # TODO Should still have a valid xml to return here!
            xml = None
            return xml, inProgress, b''

        if len(buffer) >= cls._xmlRemainingLength:
            log.warning(f'Got enough bytes {len(buffer)} >= {cls._xmlRemainingLength} to complete XML')
            # greater or equal so return what is left of the buffer
            cls._xmlBytes = cls._xmlBytes + buffer[:cls._xmlRemainingLength]

            buffer = buffer[cls._xmlRemainingLength:]
            
            xml = et.fromstring(cls._xmlBytes.decode())
            # No longer in progress so clear flags
            inProgress = ''
            cls._xmlRemainingLength = 0
            return xml, inProgress, buffer



if __name__ == "__main__":

    vmixip = '127.0.0.1'
    vmixport = 8099

    import asyncio
    
    class Handler():
        # def registerCallBacks() = None
        # print('Called!')
        def registerCallBacks(self, args,): #**kwargs):
            print(f'args:{args}') #|kwargs:{kwargs}')
    h = Handler()

    loop = asyncio.get_event_loop()

    # v = Vmix8099ClientProtocol(loop, h)
    # input = PCSScoreboardFileWatcher(args.path)

    coro = loop.create_connection(lambda: Vmix8099ClientProtocol(loop, h),
                                vmixip, vmixport)
    
    # loop.create_task(input.watch_for_file_change())
    loop.run_until_complete(coro)
    loop.run_forever()
    loop.close()

    pass