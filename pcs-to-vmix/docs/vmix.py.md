Help on module vmix:

NAME
    vmix - vMix Telnet 8099 API interface.

DESCRIPTION
    This knows about title graphics and vMix things 
    It knows nothing about cricket!

CLASSES
    asyncio.protocols.Protocol(asyncio.protocols.BaseProtocol)
        Vmix8099ClientProtocol
    builtins.object
        vmixXMLdecoder
    
    class Vmix8099ClientProtocol(asyncio.protocols.Protocol)
     |  Vmix8099ClientProtocol(eventloop, coro)
     |  
     |  The IP Session to vMix.
     |  
     |  Method resolution order:
     |      Vmix8099ClientProtocol
     |      asyncio.protocols.Protocol
     |      asyncio.protocols.BaseProtocol
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __init__(self, eventloop, coro)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  connection_lost(self, exc)
     |      Called when the connection is lost or closed.
     |      
     |      The argument is an exception object or None (the latter
     |      meaning a regular EOF is received or the connection was
     |      aborted or closed).
     |  
     |  connection_made(self, transport)
     |      Once connected get the XML.  
     |      
     |      coroutine (right name?) regsiter call backs for the field updater 
     |      - not sure these are all named in the best ways!
     |  
     |  data_received(self, byteData)
     |      Prcoess TCP session incoming data.
     |  
     |  async update_field(self, field_name, value)
     |      Field_name is the TCSldXXX or Bowl0 etc.
     |      
     |      ### TODO - use self.xml to determine if we actually need to send the update
     |      Or does this matter loadwise?
     |      Need to ensure we have an upto date xml though!
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from asyncio.protocols.Protocol:
     |  
     |  eof_received(self)
     |      Called when the other end calls write_eof() or equivalent.
     |      
     |      If this returns a false value (including None), the transport
     |      will close itself.  If it returns a true value, closing the
     |      transport is up to the protocol.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from asyncio.protocols.BaseProtocol:
     |  
     |  pause_writing(self)
     |      Called when the transport's buffer goes over the high-water mark.
     |      
     |      Pause and resume calls are paired -- pause_writing() is called
     |      once when the buffer goes strictly over the high-water mark
     |      (even if subsequent writes increases the buffer size even
     |      more), and eventually resume_writing() is called once when the
     |      buffer size reaches the low-water mark.
     |      
     |      Note that if the buffer size equals the high-water mark,
     |      pause_writing() is not called -- it must go strictly over.
     |      Conversely, resume_writing() is called when the buffer size is
     |      equal or lower than the low-water mark.  These end conditions
     |      are important to ensure that things go as expected when either
     |      mark is zero.
     |      
     |      NOTE: This is the only Protocol callback that is not called
     |      through EventLoop.call_soon() -- if it were, it would have no
     |      effect when it's most needed (when the app keeps writing
     |      without yielding until pause_writing() is called).
     |  
     |  resume_writing(self)
     |      Called when the transport's buffer drains below the low-water mark.
     |      
     |      See pause_writing() for details.
    
    class vmixXMLdecoder(builtins.object)
     |  # TODO - Make into a proper vMix_XML_Handler.  
     |  
     |  ie receivers parent substites a handler and then you always ask the handler for xml and don't have a local xml back in the client
     |  
     |  Class methods defined here:
     |  
     |  vmixXMLdecode(buffer) from builtins.type
     |      return self.xml, inProgress, buffer
     |      buffer of xml data
     |  
     |  ----------------------------------------------------------------------
     |  Readonly properties defined here:
     |  
     |  xml
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)

DATA
    log = <Logger vmix (WARNING)>

FILE
    c:\repos\pcs-to-vmix-interface\pcs-to-vmix\pcs_to_vmix\vmix.py


