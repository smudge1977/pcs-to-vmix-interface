Help on module tcpclient:

NAME
    tcpclient - The main application.

DESCRIPTION
    The user entry point
    
    ```mermaid 
    flowchart TD
    thing --> another
    ```

FUNCTIONS
    main()
        Main entry point to the utility
    
    sleep(...)
        sleep(seconds)
        
        Delay execution for a given number of seconds.  The argument may be
        a floating point number for subsecond precision.

DATA
    VERSION = '1.0.6.e99b9a9bc369adf96e93b7534276272a2fbc9c12M'
    log = <Logger tcpclient (WARNING)>

FILE
    c:\repos\pcs-to-vmix-interface\pcs-to-vmix\pcs_to_vmix\tcpclient.py


