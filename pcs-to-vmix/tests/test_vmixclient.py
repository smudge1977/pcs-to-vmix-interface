import pytest

from pcs_to_vmix.vmix import vmixXMLdecoder, Vmix8099ClientProtocol


class Test_Buffer():
    XMLQUERY = 'XMLTEXT vmix/inputs/input[@title]'
    
    # def test_A(self):
    #     buffers = [
    #         'XML 64\r\n<vmix><version>24.0.0.72</version><edition>4K</edition></vmix>'.encode()
    #     ]
    #     for item in buffers:
    #         xml, inProgress, buffer = vmixXMLdecoder.vmixXMLdecode(item)
    #     assert len(xml.getchildren()) == 2

    # def test_split1(self):
    #     buffers = [
    #         'XML 64\r\n'.encode(),
    #         '<vmix><version>24.0.0.72</version><edition>4K</edition></vmix>'.encode(),
    #     ]
    #     for item in buffers:
    #         xml, inProgress, buffer = vmixXMLdecoder.vmixXMLdecode(item)
    #     assert len(xml.decode().getchildren()) == 2


#     print()
#     pass

# t = Test_Buffer()
# t.test_A()
# print()
# pass

class Test_VmixClient():
    XML = 'XML 2705\r\n<vmix><version>24.0.0.72</version><edition>4K</edition><inputs><input key="6d825079-ac22-42a2-b727-44d354b690d6" number="1" type="GT" title="Raster.gtzip" shortTitle="Raster.gtzip" state="Paused" position="0" duration="0" loop="False" selectedIndex="0">Raster.gtzip<text index="0" name="Text5.Text">CAP Board\r\n560x512\r\n@ 1100,220</text><text index="1" name="Text4.Text">Main score board 1024x576 @ 0,0</text><text index="2" name="Text3.Text">Perimeter board 3 - 768x48 @  1100,165</text><text index="3" name="Text2.Text">Perimeter board 3 - 768x48 @  1100,110</text><text index="4" name="Text1.Text">Perimeter board 2 - 768x48 @  1100,55</text><text index="5" name="Text.Text">Perimeter board 1 - 768x48 @ 1100,0</text></input><input key="13eafd43-2f48-4f9e-b12c-9ddb931dabe5" number="2" type="GT" title="RedBallScores.gtzip" shortTitle="RedBallScores.gtzip" state="Paused" position="0" duration="0" loop="False" selectedIndex="0">RedBallScores.gtzip<text index="0" name="Text5.Text">CAP Board\r\n560x512\r\n@ 1100,220</text><text index="1" name="Score2.Text">23</text><text index="2" name="Score1.Text">23</text><text index="3" name="TCSld1.Text">Gloucestershire</text><text index="4" name="Batter2.Text">Batter2</text><text index="5" name="Batter1.Text">Batter1</text><text index="6" name="TotalScore.Text">222</text><text index="7" name="Text3.Text">Perimeter board 3 - 768x48 @  1100,165</text><text index="8" name="Text2.Text">Perimeter board 3 - 768x48 @  1100,110</text><text index="9" name="Text1.Text">Perimeter board 2 - 768x48 @  1100,55</text><text index="10" name="TCSld1_.Text">Gloucestershire</text><text index="11" name="TCSld333.Text">222 / 3</text><text index="12" name="Text.Text">Perimeter board 1 - 768x48 @ 1100,0</text></input></inputs><overlays><overlay number="1" /><overlay number="2" /><overlay number="3" /><overlay number="4" /><overlay number="5" /><overlay number="6" /><overlay number="7" /><overlay number="8" /></overlays><preview>2</preview><active>1</active><fadeToBlack>False</fadeToBlack><transitions><transition number="1" effect="Fade" duration="500" /><transition number="2" effect="Merge" duration="1000" /><transition number="3" effect="Wipe" duration="1000" /><transition number="4" effect="CubeZoom" duration="1000" /></transitions><recording>False</recording><external>False</external><streaming>False</streaming><playList>False</playList><multiCorder>False</multiCorder><fullscreen>True</fullscreen><audio><master volume="100" muted="False" meterF1="0" meterF2="0" headphonesVolume="100" /></audio><dynamic><input1></input1><input2></input2><input3></input3><input4></input4><value1></value1><value2></value2><value3></value3><value4></value4></dynamic></vmix>\r\n'
    
    def test_processXML_With_Cr(self):
        xml, inProgress, buffer = vmixXMLdecoder.vmixXMLdecode(self.XML.encode())
        assert inProgress == ''
        # buffer == '' ends with a CR/LF :shrug:
        assert len(xml.getchildren()) == 16

    def test_dataReceived(self):
        loop = input_ = None
        vmix = Vmix8099ClientProtocol(loop, input_)
        vmix.data_received(self.XML.encode())
        assert vmix._inProgress == ''
        assert len(vmix.xml.getchildren()) == 16


class Test_ProcessXML():
    BUFFER1 = 'XML 2691\r\n'
    BUFFER2 = '<vmix><version>24.0.0.72</version><edition>4K</edition><inputs><input key="6d825079-ac22-42a2-b727-44d354b690d6" number="1" type="GT" title="Raster.gtzip" shortTitle="Raster.gtzip" state="Paused" position="0" duration="0" loop="False" selectedIndex="0">Raster.gtzip<text index="0" name="Text5.Text">CAP Board\r\n560x512\r\n@ 1100,220</text><text index="1" name="Text4.Text">Main score board 1024x576 @ 0,0</text><text index="2" name="Text3.Text">Perimeter board 3 - 768x48 @  1100,165</text><text index="3" name="Text2.Text">Perimeter board 3 - 768x48 @  1100,110</text><text index="4" name="Text1.Text">Perimeter board 2 - 768x48 @  1100,55</text><text index="5" name="Text.Text">Perimeter board 1 - 768x48 @ 1100,0</text></input><input key="13eafd43-2f48-4f9e-b12c-9ddb931dabe5" number="2" type="GT" title="RedBallScores.gtzip" shortTitle="RedBallScores.gtzip" state="Paused" position="0" duration="0" loop="False" selectedIndex="0">RedBallScores.gtzip<text index="0" name="Text5.Text">CAP Board\r\n560x512\r\n@ 1100,220</text><text index="1" name="Score2.Text">23</text><text index="2" name="Score1.Text">23</text><text index="3" name="TCSld1.Text">NORTHERN</text><text index="4" name="Batter2.Text">Batter2</text><text index="5" name="Batter1.Text">Batter1</text><text index="6" name="TotalScore.Text">222</text><text index="7" name="Text3.Text">Perimeter board 3 - 768x48 @  1100,165</text><text index="8" name="Text2.Text">Perimeter board 3 - 768x48 @  1100,110</text><text index="9" name="Text1.Text">Perimeter board 2 - 768x48 @  1100,55</text><text index="10" name="TCSld1_.Text">Gloucestershire</text><text index="11" name="TCSld333.Text"></text><text index="12" name="Text.Text">Perimeter board 1 - 768x48 @ 1100,0</text></input></inputs><overlays><overlay number="1" /><overlay number="2" /><overlay number="3" /><overlay number="4" /><overlay number="5" /><overlay number="6" /><overlay number="7" /><overlay number="8" /></overlays><preview>2</preview><active>1</active><fadeToBlack>False</fadeToBlack><transitions><transition number="1" effect="Fade" duration="500" /><transition number="2" effect="Merge" duration="1000" /><transition number="3" effect="Wipe" duration="1000" /><transition number="4" effect="CubeZoom" duration="1000" /></transitions><recording>False</recording><external>False</external><streaming>False</streaming><playList>False</playList><multiCorder>False</multiCorder><fullscreen>True</fullscreen><audio><master volume="100" muted="False" meterF1="0" meterF2="0" headphonesVolume="100" /></audio><dynamic><input1></input1><input2></input2><input3></input3><input4></input4><value1></value1><value2></value2><value3></value3><value4></value4></dynamic></vmix>\r\n'

    def test_processXML2(self):
        loop = input_ = None
        xml, inProgress, buffer = vmixXMLdecoder.vmixXMLdecode(self.BUFFER1.encode())

        assert inProgress == 'XML'
        assert buffer == b''

        xml, inProgress, buffer = vmixXMLdecoder.vmixXMLdecode(self.BUFFER2.encode())

        # assert inProgress == ''
        assert len(xml.getchildren()) == 16


        assert True
    # @pytest.mark.asyncio
    # async def test_an_async_function():
    #     result = await call_to_my_async_function()
    #     assert result == 'banana'

