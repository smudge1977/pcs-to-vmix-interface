import pytest
import asyncio
import pathlib

from lxml import etree as et

PKG_ROOT = pathlib.Path(__file__).parent

from pcs_to_vmix.pcs import PCSScoreboardFileWatcher


# @pytest.mark.asyncio
# class Test_ReadFile():
#     # @pytest.mark.asyncio
#     # async def test_an_async_function():
#     #     result = await call_to_my_async_function()
#     #     assert result == 'banana'

#     async def test_exist(self):
#         tree = await read_file('./nvplay-scoreboard1.xml')
#         print(tree)
#         assert isinstance(tree, et._ElementTree)
#         assert True
    
# import pytest
# import asyncio


@pytest.fixture(scope="class")
def event_loop_instance(request):
    """ Add the event_loop as an attribute to the unittest style test class. """
    request.cls.event_loop = asyncio.get_event_loop_policy().new_event_loop()
    yield
    request.cls.event_loop.close()

# @pytest.mark.usefixtures("event_loop_instance")
# class TestTheThings(PCSScoreboardFileWatcher):
# # Example was want my class to inheriate what is been tested...
# # https://tonybaloney.github.io/posts/async-test-patterns-for-pytest-and-unittest.html#:~:text=Unittest%20classes%20from%20Pytest

#     def get_async_result(self, coro):
#         """ Run a coroutine synchronously. """
#         return self.event_loop.run_until_complete(coro)

#     def test_an_async_method(self):
        


#         assert True
#         # result = self.get_async_result(read_file(PKG_ROOT / 'nvplay-scoreboard1.xml'))
#         # # result is the actual result, so whatever assertions..
#         # # self.assertEqual(result,  "banana")
#         # print(result)
#         # assert isinstance(result, et._Element)