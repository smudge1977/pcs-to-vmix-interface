## Configure



# To work on source etc
* check out
* make venv
* install package in editable mode
* do stuff and push back to git lab
```
git checkout ....
cd pcs-to-vmix-interface
python -m venv venv
venv/Scripts/active.bat
cd pcs_to_vmix
pip install -r requirements.txt
```
When you push back git-lab 'should' sort out code checking and bumping minor version if you push to rc branch.  
This will then get pushed to PyPi test  
A human approval is then required to get to PyPi production and the second version number will get updated  

## Push to PyPi



### Create Setup / Publish

Require twine installed
```
pip install twine
```
You also need to be able to `pip install -e .` your package so have a valid `setup.py`  

Essentially you are putting a `.gz` file on a web server that contains all the assets.  
* Create the `.gz`/`.zip` (sdist - source, bdist - binary)
* Check the nicities of it like README renders on PyPi ok etc. 
* Push to PyPi test (an "rc" release?)/ procuction (a "formal" release)

```
python setup.py build
python setup.py sdist 
```
on the remote system  
`pip install pcs-to-vmix-1.0.6.tar.gz`

Not sure what is going on in the whol PyPi world at the moment!

```
# The pip install the 
# bdist
# _wheel

twine check dist/*
twine upload --repository-url https://test.pypi.org/legacy/ dist/*
twine upload dist/*
```  

# TODO
# Look at [https://pypi.org/project/bumpversion/](https://pypi.org/project/bumpversion/)

